


class STATE_TYPE(object):
    EMPTY = 'EMPTY'
    PLAYING = 'PLAYING'
    ONE_SIDE_WON = 'ONE_SIDE_WON'


class PlayingField():

    state = STATE_TYPE.EMPTY

    def _get_state(self):
        return self.state

    def get_state(self):
        return self._get_state()

    def _set_state(self, state):
        self.state = state
        return self.state

    def set_state(self, state):
        return self._set_state(state)