import json

from openpyxl import Workbook, load_workbook

from project.prg1 import PlayingField, STATE_TYPE
import aiohttp
import asyncio
import openpyxl



class City:

    def __init__(self, name, lat, long):
        self.name = name
        self.key = '1a84daddf37cab187468c292366d7053'
        self.lat = lat
        self.long = long

    def __str__(self):
        return self.name

    def _get_weather_url(self):
        return 'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={key}'.format(lat=self.lat, lon=self.long,key=self.key)

    def get_weather_url(self):
        return self._get_weather_url()

cities = [
    City('Kokeshetau', '53.2833','69.4'),
    City('Astana', '51.1801','71.446'),
    City('Almaty', '43.2567','76.9286'),

]

async def city1():
    field = PlayingField()
    field.set_state(STATE_TYPE.PLAYING)
    print(field.get_state())

    url = cities[0].get_weather_url()
    async with aiohttp.ClientSession() as session:
        #TODO make queries from db
        async with session.get(url) as resp:
            print(resp.status)
            text = await resp.text()
            print(type(text))
            wb = Workbook()
            ws = wb.active
            c = ws.cell(row=1, column=1)
            # text = json.loads(text)
            c.value = text
            wb.save('cities.xlsx')

async def city2():

    url = cities[1].get_weather_url()
    async with aiohttp.ClientSession() as session:
        #TODO make queries from db
        async with session.get(url) as resp:
            print(resp.status)
            text = await resp.text()
            print(type(text))
            wb = load_workbook('cities.xlsx')
            ws = wb.active
            c = ws.cell(row=2, column=1)
            # text = json.loads(text)
            c.value = text
            wb.save('cities.xlsx')


async def city3():
    url = cities[2].get_weather_url()
    async with aiohttp.ClientSession() as session:
        #TODO make queries from db
        async with session.get(url) as resp:
            print(resp.status)
            text = await resp.text()
            print(type(text))
            wb = load_workbook('cities.xlsx')
            ws = wb.active
            c = ws.cell(row=3, column=1)
            # text = json.loads(text)
            c.value = text
        wb.save('cities.xlsx')


loop = asyncio.get_event_loop()
loop.run_until_complete(city1())
loop.run_until_complete(city2())
loop.run_until_complete(city3())

